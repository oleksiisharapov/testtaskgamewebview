package com.testtaskapplication.testtask

import android.graphics.drawable.Animatable
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.game_activity.*

class GameActivity: AppCompatActivity(){

    private var anim: Animatable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.game_activity)

        anim = animImageView.drawable as Animatable

    }

    override fun onResume() {
        super.onResume()
        anim?.start()
    }

}