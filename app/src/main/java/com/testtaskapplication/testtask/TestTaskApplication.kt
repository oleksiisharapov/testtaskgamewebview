package com.testtaskapplication.testtask

import android.app.Application
import com.onesignal.OneSignal

class TestTaskApplication: Application(){

    override fun onCreate() {
        super.onCreate()

        OneSignal.startInit(this)
            .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
            .unsubscribeWhenNotificationsAreDisabled(true)
            .init()

    }
}