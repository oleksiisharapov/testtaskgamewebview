package com.testtaskapplication.testtask

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebView


class WebViewActivity : AppCompatActivity() {

    private lateinit var webView: WebView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_webview)
        webView = findViewById(R.id.webView)
    }


    override fun onResume() {
        super.onResume()
        webView.loadUrl("https://traffidomn.xyz/pYtnMY7B")
    }

    override fun onBackPressed() {
        if(webView.canGoBack()){
            webView.goBack()
        }else {
            super.onBackPressed()
        }
    }
}
